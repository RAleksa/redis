#include <iostream>
#include <unordered_map>
#include <fstream>
#include <string>

struct get_set_state {
    std::string key;
    std::string value;
    bool error = false;
};

void file_write(std::ofstream &file, const std::string &key, const std::string &value) {
    std::string f_key = key;
    std::string f_value = value;
    std::string key_len = std::to_string(f_key.length());
    file.write(key_len.c_str(), key_len.length());
    f_key = " " + f_key;
    file.write(f_key.c_str(), f_key.length());
    std::string value_len = std::to_string(f_value.length());
    file.write(value_len.c_str(), value_len.length());
    f_value = " " + f_value;
    file.write(f_value.c_str(), f_value.length());
}

void file_read(std::ifstream &file, std::string &key, std::string &value) {
    char str_key[1000000] = "";
    char str_value[1000000] = "";
    char c;
    
    std::string key_len_str = "";
    file.read(&c, 1);
    key_len_str += c;
    while (c != ' ' && !file.eof()) {
        file.read(&c, 1);
        key_len_str += c;
    }
    long long key_len = strtoll(key_len_str.c_str(), NULL, 10);
    file.read(str_key, key_len);
    key = (std::string) str_key;

    std::string value_len_str = "";
    file.read(&c, 1);
    value_len_str += c;
    while (c != ' ' && !file.eof()) {
        file.read(&c, 1);
        value_len_str += c;
    }
    long long value_len = strtoll(value_len_str.c_str(), NULL, 10);
    file.read(str_value, value_len);
    value = (std::string) str_value;
}

void read_database(std::unordered_map<std::string, std::string> &map_database) {
	std::ifstream database("database.txt", std::ios_base::binary);
	std::string key;
    std::string value;
	
	while (!database.eof() && database.is_open()) {
        file_read(database, key, value);
        map_database[key] = value;
    }
    database.close();
}

void write_database(std::unordered_map<std::string, std::string> &map_database) {
    std::ofstream database("database.txt", std::ios_base::binary);
    for (auto &x: map_database) {
    	file_write(database, x.first, x.second);
    }
	database.close();
}

void get(get_set_state &gss, std::unordered_map<std::string, std::string> &map_database) {
	auto got = map_database.find(gss.key);
	if (got == map_database.end()) {
		gss.error = true;
	} else {
		gss.value = got->second;
	}
}

void set(get_set_state &gss, std::unordered_map<std::string, std::string> &map_database) {
	map_database[gss.key] = gss.value;
}
