#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <iostream>
#include <algorithm>
#include <map>
#include <time.h>

#include "get_set.h"
#include "parser.h"

int main(int argc, char *argv[]) {
    try {
        if (argc != 2) {
            throw "invalid arguments";
        }
        int port = strtol(argv[1], NULL, 10);
        if (port < 0 || port > 65535) {
            throw "invalid port";
        }

        int sock;
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            throw "socket_error";
        }
        struct sockaddr_in address;

        address.sin_family = AF_INET;
        address.sin_addr.s_addr = INADDR_ANY;
        address.sin_port = htons(port);
        
        int opt = 1;
        if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
            throw "setsockopt_error";
        }

        if (bind(sock, (struct sockaddr *)&address, sizeof(address)) < 0) {
            throw "bind_error";
        }
        if (listen(sock, 8) < 0) {
            throw "listen_error";
        }
        std::cout << "listening on port " << port << "..." << std::endl;

        std::map<int, ClientState> clients;
        clients.clear();

        char rbuf[1000000];
        std::unordered_map<std::string, std::string> map_database;
        read_database(map_database);

        for (auto &x: map_database) {
            std::cout << x.first << " " << x.second << std::endl;
        }

        while (true) {
            fd_set readset;
            FD_ZERO(&readset);
            FD_SET(sock, &readset);

            for (auto i = clients.begin(); i != clients.end(); ++i)
                FD_SET(i->first, &readset);

            timeval timeout;
            timeout.tv_sec = 10;
            timeout.tv_usec = 0;
            time_t t = time(NULL);

            int mx = sock;
            if (!clients.empty()) {
                auto i = clients.end();
                --i;
                mx = std::max(mx, i->first);
            }

            int sel = select(mx + 1, &readset, NULL, NULL, &timeout);
            if (sel < 0) {
                throw "select_error";
            }

            if (time(NULL) - t > 3) {
                write_database(map_database);
            }

            if (FD_ISSET(sock, &readset)) {
                int new_sock = accept(sock, NULL, NULL);
                if (new_sock < 0) {
                    throw "accept_error";
                }

                fcntl(new_sock, F_SETFL, O_NONBLOCK);
                
                ClientState cs;
                cs.curr_state = StartState;
                clients[new_sock] = cs;
            }

            for (auto i = clients.begin(); i != clients.end(); ++i) {
                ClientState &CS = i->second;

                if (FD_ISSET(i->first, &readset)) {
                    
                    while (recv(i->first, rbuf, 1024, 0) > 0) {
                        for (int i = 0; i < sizeof(rbuf); ++i) {
                            CS.buf.push_back(rbuf[i]);
                        }
                        parse(CS);

                        if (CS.curr_state == OKState) {
                            get_set_state gss;
                            if (CS.strings.size() == 2 && CS.strings[0] == "get") {
                                gss.key = CS.strings[1];
                                get(gss, map_database);
                                if (gss.error == false) {
                                    std::string swbuf = "$" + std::to_string(gss.value.length()) + "\r\n" + gss.value + "\r\n";
                                    send(i->first, swbuf.c_str(), swbuf.size(), 0);
                                } else {
                                    std::string swbuf = "$-1\r\n";
                                    send(i->first, swbuf.c_str(), swbuf.size(), 0);
                                }
                            } else if (CS.strings.size() == 3 && CS.strings[0] == "set") {
                                gss.key = CS.strings[1];
                                gss.value = CS.strings[2];
                                set(gss, map_database);
                                std::string swbuf = "+OK\r\n";
                                send(i->first, swbuf.c_str(), swbuf.size(), 0);
                            } else {
                                std::string swbuf = "-ERR unknown command\r\n";
                                send(i->first, swbuf.c_str(), swbuf.size(), 0);
                            }
                        } else if (CS.curr_state == ErrorState) {
                            std::string swbuf = "-ERROR\r\n";
                            send(i->first, swbuf.c_str(), swbuf.size(), 0);
                        }
                    }
                    close(i->first);
                    clients.erase(i->first);
                    continue;
                }
            }

        }
        write_database(map_database);

    } catch (const char *error) {
        std::cout << error << std::endl;
    }
}