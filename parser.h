#include <deque>
#include <vector>

enum state {
    StartState,
    ArraySizeState,
    AfterArraySizeState,
    ArrayBodyState,
    StringSizeState,
    AfterStringSizeState,
    StringBodyState,
    AfterStringBodyState,
    ErrorState,
    OKState
};

struct ClientState {
    std::deque<char> buf;
    state curr_state;
    std::string s;
    std::vector<std::string> strings;
    long long aleft = 0;
    long long sleft = 0;
};

void HandleStartState(char c, ClientState &CS) {
    switch (c) {
        case '*': {
            CS.curr_state = ArraySizeState;
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleArraySizeState(char c, ClientState &CS) {
    switch (c) {
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': {
            CS.aleft = CS.aleft * 10 + (c - '0');
            break;
        }
        case '\r': {
            CS.curr_state = AfterArraySizeState;
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleAfterArraySizeState(char c, ClientState &CS) {
    switch (c) {
        case '\n': {
            CS.curr_state = ArrayBodyState;
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleArrayBodyState(char c, ClientState &CS) {
    if (!CS.aleft) {
        CS.curr_state = OKState;
        return;
    }
    switch (c) {
        case '$': {
            CS.curr_state = StringSizeState;
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleStringSizeState(char c, ClientState &CS) {
    switch (c) {
        case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9': {
            CS.sleft = CS.sleft * 10 + (c - '0');
            break;
        }
        case '\r': {
            CS.curr_state = AfterStringSizeState;
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleAfterStringSizeState(char c, ClientState &CS) {
    switch (c) {
        case '\n': {
            CS.curr_state = StringBodyState;
            CS.s = "";
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void HandleStringBodyState(char c, ClientState &CS) {
    if (CS.sleft) {
        CS.s += c;
        CS.sleft -= 1;
    } else {
        switch (c) {
            case '\r': {
                CS.strings.push_back(CS.s);
                CS.aleft -= 1;
                CS.s = "";
                CS.curr_state = AfterStringBodyState;
                break;
            }
            default: {
                CS.curr_state = ErrorState;
            }
        }
    }
}

void HandleAfterStringBodyState(char c, ClientState &CS) {
    switch (c) {
        case '\n': {
            if (CS.aleft) {
                CS.curr_state = ArrayBodyState;
            } else {
                CS.curr_state = OKState;
            }
            break;
        }
        default: {
            CS.curr_state = ErrorState;
        }
    }
}

void parse(ClientState &CS) {
    while (CS.buf.size()) {
        char c = CS.buf[0];
        CS.buf.pop_front();
        if (c == '\0') {
            return;
        }
        switch (CS.curr_state) {
            case StartState: {
                HandleStartState(c, CS);
                break;
            }
            case ArraySizeState: {
                HandleArraySizeState(c, CS);
                break;
            }
            case AfterArraySizeState: {
                HandleAfterArraySizeState(c, CS);
                break;
            }
            case ArrayBodyState: {
                HandleArrayBodyState(c, CS);
                break;
            }
            case StringSizeState: {
                HandleStringSizeState(c, CS);
                break;
            }
            case AfterStringSizeState: {
                HandleAfterStringSizeState(c, CS);
                break;
            }
            case StringBodyState: {
                HandleStringBodyState(c, CS);
                break;
            }
            case AfterStringBodyState: {
                HandleAfterStringBodyState(c, CS);
                break;
            }
        }
        if (CS.curr_state == ErrorState) {
            return;
        }
    }
}